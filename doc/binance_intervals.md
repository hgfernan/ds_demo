# Documentation of Binance Candlestick intervals

From [Public API definitions](https://binance-docs.github.io/apidocs/spot/en/#public-api-definitions), 
by [Binance API documentation](https://binance-docs.github.io/apidocs/spot/en)

## Valid suffixes


| Letter | Meaning |
| :----: | :------ |
| `s`    | seconds |
| `m`    | minutes |
| `h`    | hours   |
| `d`    | days    |
| `w`    | weeks   |
| `M`    | months  |

## Valid intervals

| Sequence | Symbol             | Meaning                               |
| -------: | :----------------- | :------------------------------------ |
|        1 | `1s`               | One second                            |
|        2 | `1m`               | One minute                            |
|        3 | `3m`               | Three minutes                         |
|        4 | `5m`               | Five minutes                          |
|        5 | `15m`              | Fifteen minutes                       |
|        6 | `30m`              | Thirty minutes                        |
|        7 | `1h`               | One hour                              |
|        8 | `2h`               | Two hours                             |
|        9 | `4h`               | Four hours                            |
|       10 | `6h`               | Six hours                             |
|       11 | `8h`               | Eight hours                           |
|       12 | `12h`              | Twelve hours                          |
|       13 | `1d`               | One day                               |
|       14 | `3d`               | Three days                            |
|       15 | `1w`               | One week                              |
|       16 | `1M`               | One month                             |

