\documentclass[a4paper,10pt]{article}
\usepackage{latexsym}
\usepackage[american]{babel}
\usepackage{lmodern}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{longtable}
%\usepackage[dvips]{graphicx}
\usepackage{graphicx}

%\usepackage[pdftex]{hyperref}
\usepackage{hyperref}
\usepackage{amsmath}  % for equation environment
\usepackage{enumitem} % nolistsep to reduce list spacing

\usepackage{parskip}% http://ctan.org/pkg/parskip

%\usepackage{lscape}
%\usepackage{pdflscape}

% define the title
\author{H.~G. Fernandes}
\title{Programs to convert OHLC data into Markov chains}
% \date{}

\frenchspacing

\pagestyle{myheadings}

\markboth{Programs to convert OHLC data into Markov chains}
         {Programs to convert OHLC data into Markov chains}

\addtolength{\hoffset}{-1,5cm}
\addtolength{\textwidth}{+3.5cm}

\addtolength{\voffset}{-1,2cm}
\addtolength{\marginparwidth}{-0,5cm}
\addtolength{\textheight}{+3.5cm}

% \linespread{1.5}


\begin{document}

\maketitle

\section{Overview}

\begin{enumerate}
    \item \label{itm:data}
        {\tt om\_getdata.py} -- get data from a exchange, and store
        it as a CSV file;

    \item \label{itm:chains}
        {\tt om\_chains.py} -- generates a Markov chain matrix from
        a fraction of differenced data, gotten from step
        \ref{itm:data};

    \item {\tt om\_test.py} -- uses a Markov chain matrix from step
        \ref{itm:chains} to test a fraction of data gotten from
        step \ref{itm:data}.

        A chart outlines the difference between observed and calculated
        values;

    \item {\tt om\_forecast.py} -- uses data from step \ref{itm:data}
        and a Markov chain matrix from step \ref{itm:chains} to forecast
        data still not available.

        A Monte Carlo algorithm can be run to chose several estimates
        and indicate average and dispersion.

        When available, the observed values can be entered to compare
        the them with the forecasted ones;


    \item {\tt gen\_mark.py} -- an unplanned, but much needed tool to
        generate test masses for {\tt om\_chains}.

\end{enumerate}

\begin{figure}[!hbp]
    \centering
    \includegraphics[scale=0.63]{img/pipeline_y.jpg}
    \caption{Simple pipeline of OHLC to Markov programs}
\end{figure}

\pagebreak
\section{Program purpose and usage description}

\subsection{{\tt om\_getdata.py} -- getting data from exchange}

\begin{tabular}{p{0.5cm}l|p{7cm}}
    \hline
    \multicolumn{2}{l|}{\tt -h, --help}
        & show this help message and exit\\
    \hline
    \multicolumn{2}{l|}{\tt -d DATA, --data DATA} & file name where
        information will be written to. {\bf Default}:
        automatic name creation\\
    \hline
    \multicolumn{2}{l|}{\tt -b BEGIN, --begin BEGIN} & start time of
        data collection at UTC timezone. {\bf Required}.\\
    \hline
    \multicolumn{2}{l|}{\tt -e END, --end END} & end time of
        data collection at UTC timezone. {\bf Required}.\\
    \hline
    \multicolumn{2}{l|}{\tt -i \{1s,1m,3m,5m,15m,30m,} & OHLC
        duration.
        \\
    & {\tt 1h,2h,4h,6h,8h,12h,1d,3d,1w,1M\}}, &
        {\bf Default}: ``1m'', meaning 1 minute. \\
    \multicolumn{2}{l|}{\tt --interval  \{1s,1m,3m,5m,15m,30m,1h,2h,}
        &\\
    & {\tt 4h,6h,8h,12h,1d,3d,1w,1M\}} & \\
    \hline
    \multicolumn{2}{l|}{\tt -p PAIR, --data PAIR} & coin pair .
        {\bf Default}: {\tt BTCUSD}\\
    \hline
\end{tabular}

The data format is always CSV. And the exchange is still only Binance.
But that can be easily changed, since most exchanges will offer OHLC
data.

There's an algorithm to create the data filename, and it is\\
{\tt d\_{\em pair}\_{\em interval}\_{\em begin}-{\em end}\_{\em
date-time}.csv}

That is: if a series of OHLC records of the pair {\tt BTCUSDT} with
intervals of 1 minute, collected data starting in the second
$1718522460$ and stopped in the second $1718582400$, starting at
1970-01-01 0:00:00 UTC, begun at 2024-06-17 17:47:00 UTC, the file name
will be

{\tt d\_BTCUSDT\_1m\_1718522460-1718582400\_20240617-174700.csv}

Internally, {\tt begin} and {\tt end} are represented in seconds since
POSIX Epoch; that is, 0th hour of January, 1st 1970. In the example
above, they begin at 2024-06-16 07:21:00 UTC and stop at 2024-06-17
00:00:00.

The time in milliseconds is convenient to the exchange. To the
convenience of humans reading the data two more columns will be added,
representing the time in the usual form.

The number of OHLC records shouldn't be larger than 1000, as that's the
limit for a Binance answer of this kind.

A future version will allow the use of date and time in the parameters.
At first only the number of seconds will be used.

\subsection{{\tt om\_chains.py} -- creation of Markov transition matrix}

\begin{tabular}{p{0.5cm}l|p{7cm}}
    \multicolumn{3}{l|}{options} \\
    \hline
    \multicolumn{2}{l|}{\tt -h, --help}
        & show this help message and exit\\
    \hline
    \multicolumn{2}{l|}{\tt -d DATA, --data DATA} & file name where
        information will be read from. {\bf Required}.\\
    \hline
    \multicolumn{2}{l|}{\tt -m MATRIX, --matrix MATRIX} & file name
        where Markov transition matrix will written to. {\bf Default}:
        automatic name creation, from the data input file.\\
    \hline
    \multicolumn{2}{l|}{\tt -n NUM\_CLASSES, --num\_classes
        NUM\_CLASSES} & number of classes for the Markov chain.\\
    &                 & {\bf Default}: 11 \\
    \hline
    \multicolumn{2}{l|}{\tt -r RATIO, --ratio RATIO} &
        ratio of elements used to train the transition matrix.
        {\bf Default}: $0.8$, equivalent to $80.0 \%$\\
    \hline
    \multicolumn{2}{l|}{\tt -s STRIDE, --stride STRIDE} &
        interval between OHLC records. {\bf Default}: 1\\
    \hline
    \multicolumn{2}{l|}{\tt -p \{Open,High,Low,Close\}} & OHLC
        price to chose from.\\
    \multicolumn{2}{l|}{\tt --price  \{Open,High,Low,Close\}}
        & {\bf Default}: ``{\tt Close}''. \\
    \hline
\end{tabular}

There's an algorithm to create the Markov transtion matrix filename,
and it is\\
{\tt m-{\em pair}\_{\em interval}\_{\em begin}-{\em end}\_{\em
date-time}\_n{\em num\_classes}\_s{\em stride}\_r{\em ratio}
\_p{\em price}.csv}

In the case being worked on, with a number of classes {\tt n} equal
to $11$, and stride {\tt s} equal to $3$, and ratio {\tt r} equal to
$0.8$, and price {\tt Close} the file name for the Markov transition
matrix is

{\tt
m\_BTCUSDT\_1m\_1718522460-1718582400\_20240617-174700\_n11\_s3\_r08\_
pClose.csv }

\subsection{{\tt om\_test.py} -- comparison of observed and calculated
    values}

\begin{tabular}{p{0.5cm}l|p{7cm}}
    \multicolumn{3}{l|}{options} \\
    \hline
    \multicolumn{2}{l|}{\tt -h, --help}
        & show this help message and exit\\
    \hline
    \multicolumn{2}{l|}{\tt -d DATA, --data DATA} & file name where
        information will be read from. {\bf Required}.\\
    \hline
    \multicolumn{2}{l|}{\tt -i INITIALIZER, --initializer INITIALIZER}
        & random number generator initial seed. {\bf Default}:
        {\tt time()} return value taken in milliseconds\\
    \hline
    \multicolumn{2}{l|}{\tt -m MATRIX, --matrix MATRIX} & file name
        where Markov transition matrix will read from. {\bf Default}:
        automatic name creation, from the data input file.\\
    \hline
    \multicolumn{2}{l|}{\tt -n NUM\_CLASSES, --num\_classes
        NUM\_CLASSES} & number of classes for the Markov chain.\\
    &                 & {\bf Default}: automatic setting, from the
                        automatic name creation, from the data input
                        file.\\
    \hline
    \multicolumn{2}{l|}{\tt -o OUTPUT, --output OUTPUT} & folder name
        where the Markdown result report will be written to.
        {\bf  Default}: automatic name creation, from the data input
        file.\\
    \hline
    \multicolumn{2}{l|}{\tt -r RATIO, --ratio RATIO} &
        ratio of elements used to train the transition matrix.
        {\bf Default}: automatic setting, from the automatic name
                       creation, from the data input file.\\
    \hline
    \multicolumn{2}{l|}{\tt -s STRIDE, --stride STRIDE} &
        interval between OHLC values. {\bf Default}: automatic
        setting, from the automatic name creation, from the data
        input file.\\
    \hline
\end{tabular}

\subsection{{\tt om\_forecast.py} -- forecast of values via Montecarlo}


\begin{tabular}{p{0.5cm}l|p{7cm}}
    \multicolumn{3}{l|}{options} \\
    \hline
    \multicolumn{2}{l|}{\tt -h, --help}
        & show this help message and exit\\
    \hline
    \multicolumn{2}{l|}{\tt -d DATA, --data DATA} & file name where
        information will be read from. {\bf Required}.\\
    \hline
    \multicolumn{2}{l|}{\tt -i INITIALIZER, --initializer INITIALIZER}
        & random number generator initial seed. {\bf Default}:
        {\tt time()} return value taken in milliseconds\\
    \hline
    \multicolumn{2}{l|}{\tt -m MATRIX, --matrix MATRIX} & file name
        where Markov transition matrix will read from. {\bf Default}:
        automatic name creation, from the data input file.\\
    \hline
    \multicolumn{2}{l|}{\tt -o OUTPUT, --output OUTPUT} & folder name
        where the Markdown result report will be written to.
        {\bf  Default}: automatic name creation, from the data input
        file.\\
    \hline
    \multicolumn{2}{l|}{\tt -f FORECAST, --forecast FORECAST} &
        Number of elements to preview.
        {\bf Default}: 60.\\
    \hline
    \multicolumn{2}{l|}{\tt -s STRIDE, --stride STRIDE} &
        interval between OHLC values. {\bf Default}: automatic
        setting, from the automatic name creation, from the data
        input file.\\
    \hline
\end{tabular}


\section{Experiences}


\end{document}
