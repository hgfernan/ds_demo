#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot OHLC as Time series, using vega-altair in streamlit

Created on Mon Mar 25 19:48:50 2024

@author: user
"""

import sys # exit()

from pathlib      import Path, PosixPath
from typing       import List, Dict
from urllib.error import URLError

# import numpy  as np   # arange()
import pandas as pd   # class dataframe
import altair as alt
import streamlit as st

from sklearn.preprocessing import MinMaxScaler

# from minibinance  import minibinance

# TODO add a prefix parameter, besides OHLC
def list_files() -> List[str]:
    result : List[str] = []
    p : Path = Path('.')

    lp : List[PosixPath] = list(p.glob('OHLC_*.csv'))
    
    result : List[str] = sorted([str(p) for p in lp])
    
    # Normal function termination
    return result
    
# TODO add a prefix parameter, besides OHLC
def get_symbol(fname : str) -> str:
    """
    Get the symbol in a CSV filename

    Parameters
    ----------
    fname : str
        A CSV filename in the form OHLC_<symbol>.<ext>

    Returns
    -------
    str
        The symbol in the filename.

    """
    result : str = ''
    
    result = fname[len('OHLC_') : ].split('.')[0]
    
    # Normal function termination
    return result

def main() -> int:
    print(f'st.session_state:\n{st.session_state}')
    # exec_count : int = 1
    
    # if st.session_state == {}:
    #     st.session_state['started'] = True
    
    # elif st.session_state.get('started'):
    #     exec_count += 1
        
    # st.write(f'Execution no {exec_count}')
        
    
    try:
        # TODO create a list of files OHLC_*.csv
        fname_list : List[str] = list_files()

        scale_data : bool = st.toggle('Scale the data')

        if scale_data:
            st.write('Data scaling activated')

            # TODO generate a table with data maximum and minimum
            
        else:
            st.write('Data scaling inactive!')
        
        # TODO if no files available, report
        
        # TODO extract the symbols from the list
        symbol_list = [ get_symbol(fname) for fname in fname_list]
        
        # TODO create a dictionary from symbols to csv files
        symbol_dict : Dict[str, str] = {}
        for ind in range(len(fname_list)):
            symbol_dict[symbol_list[ind]] = fname_list[ind]
            
        # TODO create a dropbox with all the symbols found & some default
        symbol_sel = st.multiselect(
            "Choose symbols", symbol_list, 
            [ symbol_list[ind] for ind in range(min(2, len(symbol_list)) )] 
        )
        
        if not symbol_sel:
            st.error("Please select at least one symbol.")

        else:
            chosen_list = list(symbol_sel)
            st.write(f'Chosen: {chosen_list}')
            
            date_chosen = False
            df_dict = {}
            # TODO given the chosen symbols, load files and get 'Close' for them
            for chosen in chosen_list:
                fname = symbol_dict[chosen]
                print(f'Loading file \'{fname}\'')
                
                df_aux = pd.read_csv(fname)
                
                df_dict[chosen] = df_aux['Close']
                
                if not date_chosen:
                    df_dict['Date'] = df_aux['Date']
                            
            # TODO create a dataframe with columns named as the symbols
            df = pd.DataFrame(df_dict).set_index('Date')
            print(f'df\n{df}')
            
            if scale_data: 
                # TODO get the limits to the variables 
                limits_dict = {}
                names = ['Mininum', 'Maximum']
                limits_dict[''] = names
                
                for chosen in chosen_list:
                    lim = []
                    lim.append(df[chosen].min())
                    lim.append(df[chosen].max())
                    
                    limits_dict[chosen] = lim
                
                lim = pd.DataFrame(limits_dict).set_index('')
                
                # TODO scale original data to a numpy array
                min_max_scaler = MinMaxScaler()
                arr_scaled = min_max_scaler.fit_transform(df)
                
                df = df.reset_index()
                
                # TODO recreate the data frame with the scale data
                # OBS tolist() allocates a new area to avoid conflict w/ old df
                df_dict['Date'] = df['Date'].tolist()
                
                for ind in range(len(chosen_list)):
                    df_dict[chosen_list[ind]] = arr_scaled[:, ind]
                    
                df = pd.DataFrame(df_dict)
                
                # TODO present the original data limits as a table
                st.write('### Original data limits', lim)
                
                # TODO create a dataframe with columns named as the symbols
                df = pd.DataFrame(df_dict).set_index('Date')
                print(f'df\n{df}')
                
            # TODO create a dataframe with columns named as the symbols
            # TODO change the title to reflect scaling
            st.write("### Cryptocoin prices (USDT)", df.sort_index())
                
            # TODO meld the dataframe to be plotted
            data = pd.melt(df.reset_index(), id_vars=["Date"])
            print(f'data:\n{data}')
            
            # TODO plot the dataframe
            chart = (
                alt.Chart(data)
                .mark_area(opacity=0.3)
                .encode(
                    x="Date:T",
                    y=alt.Y("value:Q", stack=None),
                    color="variable:N",
                )
            )
            st.altair_chart(chart, use_container_width=True)
        
    except URLError as e:
        st.error(
            """
            **This demo requires internet access.**
            Connection error: %s
        """
            % e.reason
        )
        
        # Return to indicate failure 
        return 1 
    
    # Normal function termination
    return 0
    
if __name__ == '__main__':
    sys.exit(main())
