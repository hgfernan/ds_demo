#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot OHLC as Time series, using vega-altair in streamlit

Created on Mon Mar 25 19:48:50 2024

@author: user
"""

import sys            # exit()
# import math           # sin()
# import random         # seed(), random()

# import numpy  as np   # arange()
import pandas as pd   # class dataframe
import altair as alt
import streamlit as st

# from typing       import List
from urllib.error import URLError

# from minibinance  import minibinance

def main() -> int:
    try:
        df = pd.read_csv('OHLC.csv')
        print(f'df:\n{df}')
            
        data = pd.melt(df[['Date', 'Open', 'Close']], id_vars=["Date"])
        print(f'data:\n{data}')
        
        chart = (
            alt.Chart(data)
            .mark_area(opacity=0.3)
            .encode(
                x="Date:T",
                y=alt.Y("value:Q", stack=None),
                color="variable:N",
            )
        )
        st.altair_chart(chart, use_container_width=True)
        
    except URLError as e:
        st.error(
            """
            **This demo requires internet access.**
            Connection error: %s
        """
            % e.reason
        )
        
        # Return to indicate failure 
        return 1 
    
    # Normal function termination
    return 0
    
if __name__ == '__main__':
    sys.exit(main())
