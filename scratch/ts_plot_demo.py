#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Time series plot demo

Created on Mon Mar 25 19:48:50 2024

@author: user
"""

# import sys            # exit()
import math           # sin()
import random         # seed(), random()

import numpy  as np   # arange()
import pandas as pd   # class dataframe
import altair as alt
import streamlit as st

from typing       import List
from urllib.error import URLError

def rnd_list(n : int) -> List[float]:
    result : List[float] = []
    
    for ind in range(n):
        result.append(random.random())
    
    # Normal function termination
    return result 

def gen_sinoid(n : int, half_ampl : float) -> List[float]: 
    freq  : float = 0.0
    phase : float = 0.0
    sinoid : List[float] = []

    x = np.linspace(start=0.0, stop=2*np.pi, num=n, endpoint=True)
    
    freq = 2*random.random()
    phase = np.pi*random.random()
    
    for xval in x:
        sinoid.append( half_ampl*math.sin(freq*xval + phase) + random.random() )
    
    # Normal function termination
    return freq, phase, sinoid

    
try:
    random.seed((1 << 23) - 1)
    # years = np.arange(1960, 2025, step=1, dtype=int).tolist()
    years = np.arange(1960, 2025, step=1).tolist()
    
    half_ampl = float(random.randint(4, 10))
    freq, phase, sinoid1 = gen_sinoid(len(years), half_ampl)
    
    print(f'sinoid 1: freq {freq}, phase {phase}')
    
    half_ampl = float(random.randint(4, 10))
    freq, phase, sinoid2 = gen_sinoid(len(years), half_ampl)
    
    print(f'sinoid 2: freq {freq}, phase {phase}')
    
    data_dict = {'year' : years, 'crypto1' : sinoid1, 
                 'crypto2' : sinoid2}
    
    df = pd.DataFrame(data_dict)
    # df = df.set_index('year')
    print(f'df:\n{df}')
    
    data = pd.melt(df, id_vars=["year"])
    print(f'data:\n{data}')
    
    chart = (
        alt.Chart(data)
        .mark_area(opacity=0.3)
        .encode(
            x="year:Q",
            # x="year:T",
            # x="year",
            y=alt.Y("value:Q", stack=None),
            color="variable:N",
        )
    )
    st.altair_chart(chart, use_container_width=True)
    
except URLError as e:
    st.error(
        """
        **This demo requires internet access.**
        Connection error: %s
    """
        % e.reason
    )
