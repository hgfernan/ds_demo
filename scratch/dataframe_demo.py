#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Work in progress of a data science demo code

Created on Fri Mar 22 21:27:28 2024

@author: `streamlit` documentation crew
"""

import sys            # exit()
import pandas as pd   # class dataframe
import altair as alt
import streamlit as st

from urllib.error import URLError

@st.cache_data
def get_UN_data():
    AWS_BUCKET_URL = "https://streamlit-demo-data.s3-us-west-2.amazonaws.com"
    df = pd.read_csv(AWS_BUCKET_URL + "/agri.csv.gz")
    return df.set_index("Region")

def main() -> int:
    try:
        df = get_UN_data()
        country_list = sorted(list(df.index))
        countries = st.multiselect(
            "Choose countries", country_list, ["China", "Brazil"]
        )
        if not countries:
            st.error("Please select at least one country.")
        else:
            print(f'type(countries): {type(countries)}')
            print(f'countries:\n{countries}')
            
            data = df.loc[countries]
            data /= 1000000.0
            st.write("### Gross Agricultural Production ($B)", data.sort_index())
            
            print(f'data:\n{data}')
    
            data = data.T.reset_index()
            print(f'resetted data:\n{data}')
            
            data = pd.melt(data, id_vars=["index"]).rename(
                columns={"index": "year", "value": "Gross Agricultural Product ($B)"}
            )
            print(f'melted data:\n{data}')

            chart = (
                alt.Chart(data)
                .mark_area(opacity=0.3)
                .encode(
                    x="year:T",
                    y=alt.Y("Gross Agricultural Product ($B):Q", stack=None),
                    color="Region:N",
                )
            )
            st.altair_chart(chart, use_container_width=True)
    except URLError as e:
        st.error(
            """
            **This demo requires internet access.**
            Connection error: %s
        """
            % e.reason
        )
        
        # Return to indicate failure
        return 1
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())
