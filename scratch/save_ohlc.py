#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tiny little program to save a Binance OHLC as a CSV

Created on Tue Mar 26 19:23:06 2024

@author: user
"""

import sys # exit()
import datetime # class datetime

# import numpy  as np # array
# import pandas as pd # DataFrame

from typing import List

from minibinance import MiniBinance

def save_ohlc(symbol = "BTCUSDT", interval="1d", limit=31) -> bool:
    print(f'OHLC for {symbol}')
    binance = MiniBinance()
    
    try:
        ohlc = binance.get_ohlc(symbol, interval="1d", limit=31)
        ohlc['Date'] = ohlc['Timestamp'].\
            map(lambda x: datetime.date.fromtimestamp(float(x) / 1000))
        ohlc = ohlc.set_index(['Timestamp'])
        
        print(ohlc)
        ohlc.to_csv(f'OHLC_{symbol}.csv')
        
    except Exception as exc:
        print(f'{type(exc)} : {exc}')
        
        # Return to indicate failure
        return False
    
    # Normal function termination
    return True

def main(argv : List[str]) -> int:
    # binance = MiniBinance()

    try:
        # print('OHLC')
        # ohlc = binance.get_ohlc("BTCUSDT", interval="1d", limit=31)
        # print(f'{ohlc}')
        
        # # oh_array = np.array(ohlc)
        
        # # df_dict = {}
        # # timestamp = oh_array[:, 0]
        # # df_dict['Timestamp'] = timestamp
        # # df_dict['Open'     ] = oh_array[:, 1]
        # # df_dict['High'     ] = oh_array[:, 2]
        # # df_dict['Low'      ] = oh_array[:, 3]
        # # df_dict['Close'    ] = oh_array[:, 4]
        # # df_dict['Volume'   ] = oh_array[:, 5]
        
        
        # # df = pd.DataFrame(df_dict)
        # # # df = df.set_index(['Timestamp'])
        # # # df.index.names = ['timestamp']
        # # # df['Date'] = \
        # # #     df.map(lambda x: print(x['Timestamp']))
        # ohlc['Date'] = ohlc['Timestamp'].\
        #     map(lambda x: datetime.date.fromtimestamp(float(x) / 1000))
        # ohlc = ohlc.set_index(['Timestamp'])
      
        # print(ohlc)
        # print(ohlc.columns)
        
        # ohlc.to_csv('OHLC.csv')
        # print(ohlc.index)
        
        result = 0 
        symbols = ['BTCUSDT', 'ETHUSDT', 'BNBUSDT', 'LTCUSDT', 'ADAUSDT', 
                   'XRPUSDT']
        for symbol in symbols:
            rv = save_ohlc(symbol=symbol, interval='1d', limit = 31)
            if not rv:
                print(f'{argv[0]}: Could not create CSV for {symbol}')
                result += 1
                
            if result > 0:
                print(f'{argv[0]}: Failures creating the CSV')
                
                # Return to indicate failure
                return result
            
    except Exception as exc:
        print(f'{type(exc)} : {exc}')
        
        # Return to indicate failure
        return 1
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

