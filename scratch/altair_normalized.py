#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code of normalized plot
Created on Mon Mar 25 20:30:09 2024

@author: Vega-Altair documentation
From: 
*  https://altair-viz.github.io/gallery/normalized_stacked_area_chart.html
"""

import sys            # exit()
# import pandas as pd   # class dataframe
import altair as alt
import streamlit as st

from urllib.error import URLError
from vega_datasets import data

def main() -> int:
    try:
        source = data.iowa_electricity()
        
        print(f'source:\n{source}')

        chart = (
            alt.Chart(source).mark_area().encode(
                x="year:T",
                y=alt.Y("net_generation:Q").stack(None),
                color="source:N"
            )
        )  
        st.altair_chart(chart, use_container_width=True)
    
    except URLError as e:
        st.error(
            """
            **This demo requires internet access.**
            Connection error: %s
        """
            % e.reason
        )
        
        # Return to indicate failure
        return 1
    
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())