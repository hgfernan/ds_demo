# Proposta de trabalho conjunto

Segue uma proposta de trabalho conjunto para duas pessoas, na qual uma das pessoas fará o trabalho mais rotineiro, permitindo que a outra pessoa possa fazer o trabalho criativo e assim se aperfeiçoar em conhecimento, gerando um *portfolio* que poderá ser apresentado para avaliadores de emprego.

Este sistema, que chamaremos inicialmente de ***Dashboard* financeira** se destinará a apresentar dados financeiros de modo gráfico e ou resumido, segundo duas situações: uma única variável financeira, ou várias delas.

As estatísticas e diagramas serão uma vitrine que demonstrará para avaliadores um pouco do conhecimento e habilidades da pessoa criativa do grupo.

Sendo um pequeno sistema para demonstração -- um *demo* -- a prioridade é a simplicidade, para que possa ser rapidamente terminado e apresentado.

- [Proposta de trabalho conjunto](#proposta-de-trabalho-conjunto)
  - [Metáforas são forma simples para apresentar temas](#metáforas-são-forma-simples-para-apresentar-temas)
  - [Ideia geral de programa de trabalho](#ideia-geral-de-programa-de-trabalho)
    - [Algoritmo e visão geral](#algoritmo-e-visão-geral)
    - [Sistemas e recursos a utilizar](#sistemas-e-recursos-a-utilizar)
  - [Arquitetura da ***Dashboard* financeira**](#arquitetura-da-dashboard-financeira)

## Metáforas são forma simples para apresentar temas

Um recurso muito usado por professores é usar uma situação cotidiana simples como base de uma explicação conceitual. Nas técnicas de projeto de sistemas este recurso recebe o nome de *metáfora*.

Sendo assim, usaremos como metáfora a criação de um bolo de aniversário do tipo caseiro. Quem quer que tenha acompanhado uma dona de casa das antigas verá que há duas etapas bastante distintas na criação de um bolo de aniversário. Ele é composto de duas partes bem distintas: um bolo básico, chamado em geral de *pão de ló*, recheio e cobertura.

A rigor, a arte, a inovação e o sabor estão no recheio e na cobertura. Em si, o pão de ló não tem sabor distintivo: não é à toa que recebe o nome de "pão".

Mas sem o pão de ló não há bolo. Ele é usado para criar a estrutura do bolo. E, neste ponto, é cortado e algumas vezes moldado impiedosamente, para que se possa construir a estrutura artística do bolo.

Neste caso, o "pão de ló" serão rotinas básicas que uma das pessoas do trabalho conjunto deverá entregar para que a outra pessoa possa fazer seu trabalho criativo de unir as diversas partes e assim criar um programa de demonstração, ou *demo*, capaz de impressionar avaliadores de entrevistas de emprego.

Além disso, a pessoa encarregada das rotinas, do "pão de ló" se compromete a ajudar solução de questões pontuais de infraestrutura do pequeno sistema sendo desenvolvido. Dentro da metáfora, isto seria equivalente a, por exemplo, produzir mais chantily, ou mais recheio para o bolo. Ou talvez conseguir chantily ou recheio diferentes. Em qualquer caso, produzir elementos para que o integrador do sistema possa realizar seu trabalho criativo.

De qualquer modo, para facilitar o trabalho conjunto, as duas pessoas envolvidas terão visão geral do sistema ***Dashboard* financeira** e procurarão discutir conjuntamente as alternativas técnicas envolvidas no trabalho.

## Ideia geral de programa de trabalho

Nesta seção se define em termos muito gerais como deverá operar a ***Dashboard* financeira**

### Algoritmo e visão geral

1. O programa permitirá escolher variáveis a serem exibidas, dentro de um conjunto pré-fixado. A princípio, pela facilidade de obter dados, estão sendo consideradas
    cotações de ações em bolsa brasileira, e preços de criptomoedas em dólar.

    Um ponto importante é que as escolhas deverão ser simples e não exigir processamento. Afinal, neste momento, trata-se de um *demo*, não de um programa em produção: o objetivo é oferecer uma *dashboard* financeira fácil de operar, fácil de entender, e fácil de implementar.

    Neste caso, a princípio, os conjuntos de ações em bolsa e criptomoedas serão disjuntos; ou seja: neste momento não se tentará relacionar cotações de ações com preços de criptomoedas. E, ainda, apesar de haver várias medidas possíveis, associadas às fontes de dados, propõe-se que seja, neste momento, fixada apenas poucas delas.

    Neste momento, propõe-se cotações de ações ao final de um dia de negociação, e preços de criptomoedas no fechamento de um dia de negociação. Naturalmente, esta é apenas uma proposta simplificadora -- caso haja necessidade de outros recursos, pode-se repensar as propostas.

2. Se for escolhida apenas **uma** das fontes de dados, ela será tratada como uma série temporal. E, então serão mostrados diversos tratamentos estatísticos relativos a
   séries temporais ou conjuntos unitários de variáveis;

3. Se forem escolhidas **várias** fontes de dados, serão aplicadas a elas estatísticas multivariadas. Com a possibilidade, então de indicar qual é a variável
   *dependente*, que se quer explicar em função das outras.

### Sistemas e recursos a utilizar

Um princípio básico é utilizar recursos de programação, que estejam disponíveis como Software Livre, também conhecido como *open source*. A linguagem Python, muito usada em *data science* é um pré-requisito. Para facilitar a visualização por terceiros, a solução deverá estar disponível na Web. O que há algum tempo implicaria em usar também JavaScript -- possivelmente através de `node.js`. O que levaria a muitos problemas técnicos novos e, portanto,

Felizmente, há bibliotecas e *frameworks* para Python que geram páginas de *dashboard* bastante apresentáveis. Duas delas estão sendo consideradas:

- [streamlit](https://streamlit.io/) é uma excelente ferramenta para criar *dashboards*, com grau impressionante de abrangência e flexibilidade.
  
  Por exemplo, tem já desenhado todo um conjunto de recursos para teste. Além disso, oferece fácil integração com as mais importantes bibliotecas Python para visualização. Apesar de, aparentemente, ter relação privilegiada com [altair](https://altair-viz.github.io/), formalmente conhecida como `vega-altair`;

- [flet](https://flet.dev/) é uma biblioteca que permite criar com facilidade aplicativos Web em Python. Mas, sendo baseada na linguagem [flutter](https://flutter.dev/)
   permite criar também aplicações para vários sistemas operacionais -- inclusive Android.

## Arquitetura da ***Dashboard* financeira**

Aparentemente, é possível criar todo um conjunto de elementos de tela configuráveis em `streamlit`. Mas,considerando que uma aplicação *dashboard* deve ter uma tela simples e fácil de entender. E, ainda, entendendo que algumas etapas podem ser demoradas, considera-se neste momento dividir a aplicação em duas partes: configuração e obtenção de dados, e visualização.

1. **configuração e obtenção de dados**: nesta etapa, através de uma aplicação *flet* é selecionada uma fonte dados -- por exemplo, a bolsa brasileira B3, ou uma
   bolsa de criptomoedas, como a Binance.

   Os dados são transferidos e salvos em um diretório, no formato CSV, com nomes padronizados, para fácil recuperação. A gravação em CSV permite alguma estabilidade nos resultados, que podem mudar rapidamente nos mercados financeiros, e facilita diversas experimentações, na etapa seguinte.

   Os arquivos CSV, conforme mencionado, são obtidos de registros OHLC -- os tradicionais *Open, High, Low, and Close*.

   Uma vez selecionados os dados, o usuário pode passar para sua visualização;

2. **visualização**: nesta etapa as fontes de dados selecionadas na etapa anterior ficam disponíveis pelo nome de suas variáveis. A princípio, ficam apenas disponíveis
   os dados de fechamento, ou *Close*. Mas isto pode ser reavaliado, se necessário.

   Há dois tipos de visualização: caso seja escolhida uma única fonte de dados, ou caso sejam escolhidas múltiplas fontes de dados.

   A título de exemplo, seguem algumas análises estatísticas para o caso de uma única fonte de dados e múltiplas fontes de dados:

   - **Única fonte de dados**:
  
     - [Decomposição da série temporal em fatores](https://en.wikipedia.org/wiki/Decomposition_of_time_series): identificação de componentes de tendência,
         sasonal, cíclico e ruído;

     - [Densidade espectral](https://pt.wikipedia.org/wiki/Densidade_espectral) permite avaliar poder relativo de frequências;
  
     - Diagrama ACF, ou *autocorrelation function*: estimativa da autocorrelação da variável gerada pela fonte de dados para diferentes intervalos, ou *lags*;

     - Diagrama PACF, ou *partial autocorrelation function*: estimativa da [correlação parcial](https://en.wikipedia.org/wiki/Partial_autocorrelation_function)
      para diferentes intervalos;

     - [Teste de raiz unitária](https://en.wikipedia.org/wiki/Unit_root) permite definir se uma variável aleatória é estacionária ou não;
  
     - [Teste de homogeneidade de variância](https://en.wikipedia.org/wiki/Homoscedasticity_and_heteroscedasticity#Testing) para definir se diferentes
      segmentos da variável têm diferentes variâncias;

     - Várias formas de previsão ou *forecasting* de valores: ARIMA, SARIMA, LSTM e [Prophet](https://facebook.github.io/prophet/)
  
   - **Múltiplas fontes de dados**:

      - Correlação de variáveis do tipo Pearson para criar mapas de calor, ou *heat maps*;
      - Quadrado da correlação, para criar mapas de calor que são independentes de sinal;
      - Análiise de agrupamento das variáveis, com representação bidimensional;
      - Análise de componentes principais, ou *principal component analysis, ou PCA;
      - *Random forest* ou floresta aleatória, para estimar valores de uma variável dependente, em função de outras, consideradas independentes;
      - Várias outras formas de previsão ou *forecasting* de valores, como regressão múltipla e LSTM.

   Será dada prioridade às análises estatísticas que ofereçam visualização na forma de gráficos ou imagens.
